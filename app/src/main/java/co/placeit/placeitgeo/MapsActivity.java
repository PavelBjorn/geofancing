package co.placeit.placeitgeo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.pavel.placitlocation.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import co.placeit.placeitgeo.data.models.GeofenceModel;
import co.placeit.placeitgeo.data.settings.SettingHelper;
import co.placeit.placeitgeo.events.LocationEvent;
import co.placeit.placeitgeo.service.LocationService;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<GeofenceModel> mGeofences;
    private Marker locationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SettingHelper.init(this.getApplicationContext());
        SettingHelper.getInstance().setGeofences(getString(R.string.events));
        mGeofences = new Gson().fromJson(SettingHelper.getInstance().getGeofences(), new TypeToken<List<GeofenceModel>>() {
        }.getType());
        Intent intent = new Intent(MapsActivity.this, LocationService.class);
        stopService(intent);
        startService(intent);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        for (GeofenceModel model : mGeofences) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(model.getLat(), model.getLng())).title(model.getId()));
            mMap.addCircle(new CircleOptions().center(new LatLng(model.getLat(), model.getLng()))
                    .radius(model.getRadius())
                    .strokeColor(Color.RED)
                    .fillColor(getResources().getColor(R.color.eventZoneColor)));
        }
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setMyLocationEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        attachEventBus();
    }

    @Override
    protected void onStop() {
        dettachEventBus();
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationEvent(LocationEvent event) {
        if (mMap == null) return;
        LatLng currentCoordinates = new LatLng(event.mCurrentLocation.getLatitude(), event.mCurrentLocation.getLongitude());

        if (locationMarker != null) {
            locationMarker.setPosition(currentCoordinates);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentCoordinates, 20));
            return;
        }

        locationMarker = mMap.addMarker(new MarkerOptions()
                .position(currentCoordinates).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)));
    }

    private void attachEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void dettachEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
