package co.placeit.placeitgeo.data.api.battery;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 01.03.2017.
 */

public class BatteryApiService {

  private BatteryApi mBatteryApi;
  private static final String BASE_URL = "http://placeit.jellyworkz.com/";

  private BatteryApiService() {
    Gson gson = new GsonBuilder()
      .setLenient()
      .create();


    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    mBatteryApi = new Retrofit.Builder()
      .baseUrl(BASE_URL)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .client(httpClient.build())
      .build().create(BatteryApi.class);
  }

  public BatteryApi getBatteryApi() {
    return mBatteryApi;
  }

  public static BatteryApiService newInstance() {
    return new BatteryApiService();
  }
}
