package co.placeit.placeitgeo.data.settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


public class SettingHelper {

  private SharedPreferences mPreferences;
  private static SettingHelper mInstance;
  private static final String PREF_NAME = "location_config";


  private SettingHelper(Context context) {
    mPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
  }

  public static void init(Context context) {
    if (mInstance == null) {
      mInstance = new SettingHelper(context);
    }
  }

  public static SettingHelper getInstance() {
    return mInstance;
  }

  @SuppressLint("CommitPrefEdits")
  public void setUserId(long userId) {
    mPreferences.edit().putLong(Keys.USER_ID, userId).commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setToken(String token) {
    mPreferences.edit().putString(Keys.TOKEN, token).commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setAuthenticationData(String token, long userId) {
    mPreferences.edit().putString(Keys.TOKEN, token).putLong(Keys.USER_ID, userId).commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setAuthenticationData(String token, long userId, String kidName) {
    if (TextUtils.isEmpty(kidName)) {
      setAuthenticationData(token, userId);
      return;
    }
    mPreferences.edit()
      .putString(Keys.TOKEN, token)
      .putLong(Keys.USER_ID, userId)
      .putString(Keys.KID_NAME, kidName)
      .commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setServiceActive(boolean serviceActive) {
    mPreferences.edit().putBoolean(Keys.IS_SERVICE_ACTIVE, serviceActive).commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setTrackingInterval(long timeMillis) {
    mPreferences.edit().putLong(Keys.LOCATION_INTERVAL, timeMillis).commit();
  }

  @SuppressLint("CommitPrefEdits")
  public void setMinTrackingDistance(float distanceMeters) {
    mPreferences.edit().putFloat(Keys.LOCATION_MIN_DISTANCE, distanceMeters).commit();
  }

  public void setGeofences(String geo) {
    mPreferences.edit().putString("geo", geo).commit();
  }

  public String getGeofences() {
    return mPreferences.getString("geo", "");
  }

  public long getTrackingInterval() {
    return mPreferences.getLong(Keys.LOCATION_INTERVAL, DefaultValues.DEFAULT_LOCATION_INTERVAL_MILLIS);
  }

  public float getMinTrackingDistance() {
    return mPreferences.getFloat(Keys.LOCATION_MIN_DISTANCE, DefaultValues.DEFAULT_LOCATION_DISTANCE_METERS);
  }

  public boolean isServiceActive() {
    return mPreferences.getBoolean(Keys.IS_SERVICE_ACTIVE, false);
  }

  public void setFirstTrackingBattery(boolean firstTrackingBattery) {
    mPreferences.edit().putBoolean(Keys.FIRST_BATTERY_START, firstTrackingBattery).commit();
  }

  public boolean getFirstBatteryStart() {
    return mPreferences.getBoolean(Keys.FIRST_BATTERY_START, false);
  }

  public long getUserId() {
    return mPreferences.getLong(Keys.USER_ID, -1);
  }

  public String getToken() {
    return mPreferences.getString(Keys.TOKEN, "");
  }

  public String getKidName() {
    return mPreferences.getString(Keys.KID_NAME, "");
  }

  public int getTestNitificationId() {
    int id = mPreferences.getInt("testId", 1);
    mPreferences.edit().putInt("testId", id + 1).commit();
    return id;
  }


  public interface Keys {
    String IS_SERVICE_ACTIVE = "isServiceActive";
    String LOCATION_INTERVAL = "interval";
    String LOCATION_MIN_DISTANCE = "min_distance";
    String USER_ID = "user_id";
    String TOKEN = "token";
    String FIRST_BATTERY_START = "battery";
    String KID_NAME = "kid_name";
  }

  interface DefaultValues {
    int DEFAULT_LOCATION_INTERVAL_MILLIS = 1000; //* 60;
    float DEFAULT_LOCATION_DISTANCE_METERS = 1f;
  }
}
