package co.placeit.placeitgeo.data.api;

import com.google.gson.annotations.SerializedName;


/**
 * Created by User on 15.02.2017.
 */

public class RequestModel {

  @SerializedName("latitude")
  private double mLat;

  @SerializedName("longitude")
  private double mLng;


  private RequestModel(double lat, double lng) {
    mLat = lat;
    mLng = lng;
  }

  public static RequestModel create(double lat, double lng) {
    return new RequestModel(lat, lng);
  }

}
