package co.placeit.placeitgeo.data.api;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by User on 07.02.2017.
 */
public interface LocationApi {

  @POST("api/user/event/track_location")
  Call<Response> sendLocation(@Header("user-id") long userId
          , @Header("token") String token
          , @Body List<RequestModel> body);

  @FormUrlEncoded
  @POST("api/user/event/track_location")
  Call<Response> sendLocation(@Header("user-id") long userId
          , @Header("token") String token
          , @Field("event_id") String eventId
          , @Field("status") boolean status);

}
