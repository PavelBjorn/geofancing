package co.placeit.placeitgeo.data.api.battery;

import com.google.gson.annotations.SerializedName;

public class BatteryRequest {

  @SerializedName("type")
  private String mType = "5";

  @SerializedName("data")
  private DataModel mDataModel;


  private BatteryRequest(DataModel dataModel) {
    mDataModel = dataModel;
  }

  public static BatteryRequest create(String kidName, String value) {
    return new BatteryRequest(new DataModel(kidName, value));
  }

  private static class DataModel {

    @SerializedName("kid_name")
    private String mKidName;

    @SerializedName("value")
    private String mValue;

    DataModel(String kidName, String value) {
      mKidName = kidName;
      mValue = value;
    }

  }
}
