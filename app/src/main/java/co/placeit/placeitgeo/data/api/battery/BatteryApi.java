package co.placeit.placeitgeo.data.api.battery;


import co.placeit.placeitgeo.data.api.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by User on 01.03.2017.
 */

public interface BatteryApi {

  @POST("api/user/notification")
  Call<Response> postBattery(@Header("user-id") long userId
          , @Header("token") String token
          , @Body BatteryRequest batteryRequest);

}
