package co.placeit.placeitgeo.data.models;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.gson.annotations.SerializedName;

import co.placeit.placeitgeo.service.LocationService;


/**
 * Created by User on 15.03.2017.
 */

public class GeofenceModel {

    @SerializedName("id")
    private String mId;

    @SerializedName("lat")
    private float mLat;

    @SerializedName("lng")
    private float mLng;

    @SerializedName("radius")
    private int mRadius;

    private boolean isCheckedIn = false;

    public String getId() {
        return mId;
    }

    public float getLat() {
        return mLat;
    }

    public float getLng() {
        return mLng;
    }

    public int getRadius() {
        return mRadius;
    }

    public Location getLocation() {
        Location location = new Location("");
        location.setLatitude(mLat);
        location.setLongitude(mLng);
        return location;
    }

    public void setCheckedIn(boolean checkedIn) {
        isCheckedIn = checkedIn;
    }

    public boolean isCheckedIn() {
        return isCheckedIn;
    }

    public boolean isStatusChanged(Location location) {
        boolean status = location.distanceTo(getLocation()) <= mRadius;

        Log.d(LocationService.LOG_TAG, "Geofance " + mId
                + ", Radius = " + mRadius
                + ", Distance " + location.distanceTo(getLocation())
                + ", Status " + (status ? "Enter" : "Exit"));

        if (isCheckedIn == status) return false;
        setCheckedIn(status);
        return true;
    }

    public Geofence createGeofence() {
        return new Geofence.Builder().setRequestId(mId)
                .setCircularRegion(mLat, mLng, mRadius != 0 ? mRadius : 200)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build();
    }

}
