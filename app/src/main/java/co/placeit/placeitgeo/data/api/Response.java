package co.placeit.placeitgeo.data.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 09.02.2017.
 */

public class Response {

  @SerializedName("success")
  private boolean mSuccesses;

  @SerializedName("error")
  private String mError;


  public boolean isSuccesses() {
    return mSuccesses;
  }

  public String getErrorDescription() {
    return mError;
  }
}
