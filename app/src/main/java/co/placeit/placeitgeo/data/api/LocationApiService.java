package co.placeit.placeitgeo.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 07.02.2017.
 */

public class LocationApiService {

  private LocationApi mLocationApi;
  private static final String BASE_URL = "http://placeit.jellyworkz.com/";

  private LocationApiService() {
    Gson gson = new GsonBuilder()
      .setLenient()
      .create();


    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    mLocationApi = new Retrofit.Builder()
      .baseUrl(BASE_URL)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .client(httpClient.build())
      .build().create(LocationApi.class);
  }

  public LocationApi getLocationApi() {
    return mLocationApi;
  }

  public static LocationApiService newInstance() {
    return new LocationApiService();
  }

}
