package co.placeit.placeitgeo.events;

import android.location.Location;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by pavel on 22.07.2017.
 */

public class LocationEvent {
    public final Location mCurrentLocation;

    private LocationEvent(Location currentLocation) {
        mCurrentLocation = currentLocation;
    }

    public static void post(Location currentLocation){
        EventBus.getDefault().post(new LocationEvent(currentLocation));
    }
}
