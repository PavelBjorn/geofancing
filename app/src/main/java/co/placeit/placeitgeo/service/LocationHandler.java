package co.placeit.placeitgeo.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.placeit.placeitgeo.data.api.LocationApiService;
import co.placeit.placeitgeo.data.api.Response;
import co.placeit.placeitgeo.data.models.GeofenceModel;
import co.placeit.placeitgeo.data.settings.SettingHelper;
import co.placeit.placeitgeo.events.LocationEvent;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by pavel on 22.07.2017.
 */

public class LocationHandler implements GoogleApiClient.ConnectionCallbacks {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private List<GeofenceModel> mGeofenceModels = new ArrayList<>();
    private LocationApiService mLocationApiService = LocationApiService.newInstance();
    private Context mContext;
    public static final long TRACKING_INTERVAL = 1 * 1000 * 30; // min
    public static final String TAG = "LocationHandler";


    LocationHandler(Context context, GeofenceModel... models) {
        SettingHelper.init(context.getApplicationContext());
        mContext = context.getApplicationContext();
        initGoogleApiClient();
        connectGoogleApiClient(models);
    }

    private void initLocationManager() {
        LocationManager mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        if (mLocationManager == null || mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d(TAG, "GPS_PROVIDER enable");
        }

        if (mLocationManager == null || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.d(TAG, "NETWORK_PROVIDER enable ");
            return;
        }

        Log.d(TAG, "Location providers unavailable");
    }

    boolean connectGoogleApiClient(GeofenceModel... models) {
        initLocationManager();
        if (models == null || !(models.length > 0)) {
            return false;
        }
        mGeofenceModels.addAll(Arrays.asList(models));
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            return true;
        }
        return false;
    }

    void disconnectGoogleAdiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.disconnect();
        }
    }

    private void stopTracking() {
        Log.d(TAG, "stopTracking() called");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
        }
    }

    private void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(TRACKING_INTERVAL);
        locationRequest.setInterval(TRACKING_INTERVAL);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, mLocationListener);
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void detach() {
        stopTracking();
        disconnectGoogleAdiClient();
        mLocationListener = null;
        mContext = null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected() called with: bundle = [" + bundle + "]");
        createLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called with: i = [" + i + "]");
    }

/*    private void sendTestNotification(Context context, String title, String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentText(message)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(Notification.PRIORITY_MAX);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(SettingHelper.getInstance().getTestNitificationId(), mBuilder.build());
    }*/

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            // TODO: 22.07.2017 remove it later (add just for test)
            Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]");
            LocationEvent.post(location);
            location.getAltitude();
            for (GeofenceModel geofenceModel : mGeofenceModels) {
                if (geofenceModel.isStatusChanged(location)) {
                    SettingHelper.getInstance().setGeofences(new Gson().toJson(mGeofenceModels));
                   /* sendTestNotification(mContext.getApplicationContext(), geofenceModel.getId()
                            , geofenceModel.isCheckedIn() ? "Enter" : "Exit");
*/
                    /* sendRequest(geofenceModel);*/
                }
            }
        }
    };

    private void sendRequest(final GeofenceModel geofenceModel) {
        mLocationApiService.getLocationApi().sendLocation(SettingHelper.getInstance().getUserId()
                , SettingHelper.getInstance().getToken()
                , geofenceModel.getId()
                , geofenceModel.isCheckedIn()
        ).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.d(TAG, t.toString());
                // TODO: 22.07.2017 implement logic to resend geofance if was failed
                /*geofenceModel.setCheckedIn(false);*/
            }
        });
    }
}
