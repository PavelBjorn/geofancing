package co.placeit.placeitgeo.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import co.placeit.placeitgeo.data.models.GeofenceModel;
import co.placeit.placeitgeo.data.settings.SettingHelper;


public class LocationService extends Service {

    private LocationHandler mLocationHandler;
    public static final String LOG_TAG = "LocationService";

    public LocationService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SettingHelper.init(getApplicationContext());
        SettingHelper.getInstance().setServiceActive(true);
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SettingHelper.init(getApplicationContext());
        SettingHelper.getInstance().setServiceActive(true);

        detachLocationHandler();
        String gefencesJson = SettingHelper.getInstance().getGeofences();

        if (TextUtils.isEmpty(gefencesJson)) {
            Log.d(LOG_TAG, "onStartCommand() no events");
            stopSelf();
        } else {
            try {
                GeofenceModel[] geofenceModels = new Gson().fromJson(gefencesJson
                        , new TypeToken<GeofenceModel[]>() {
                }.getType());
                Log.d(LOG_TAG, "onStartCommand()");
                mLocationHandler = new LocationHandler(this, geofenceModels);
            } catch (Throwable e) {
                Log.d(LOG_TAG, "onStartCommand() called " + e);
            }
        }

        return super.onStartCommand(intent, START_FLAG_RETRY, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");
        SettingHelper.getInstance().setServiceActive(false);
        detachLocationHandler();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(LOG_TAG, "onTaskRemoved");
        SettingHelper.getInstance().setServiceActive(false);
        detachLocationHandler();
        super.onTaskRemoved(rootIntent);
    }

    private void detachLocationHandler() {
        if (mLocationHandler != null) {
            mLocationHandler.detach();
            mLocationHandler = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        detachLocationHandler();
        super.finalize();
    }
}

