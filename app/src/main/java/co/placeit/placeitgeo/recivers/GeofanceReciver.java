package co.placeit.placeitgeo.recivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import co.placeit.placeitgeo.data.api.LocationApiService;
import co.placeit.placeitgeo.data.api.Response;
import co.placeit.placeitgeo.data.settings.SettingHelper;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by User on 14.06.2017.
 */

public class GeofanceReciver extends BroadcastReceiver {

  private static final String LOG_TAG = "GeofanceReciver";
  private GeofencingEvent geofencingEvent;
  private LocationApiService api;

  @Override
  public void onReceive(final Context context, Intent intent) {
    SettingHelper.init(context.getApplicationContext());
    try {
      geofencingEvent = GeofencingEvent.fromIntent(intent);
      Log.d(LOG_TAG, "geofencingEvent = [" + geofencingEvent + "], intent = [" + intent + "]");

      if (api == null) api = LocationApiService.newInstance();

      for (Geofence geofence : geofencingEvent.getTriggeringGeofences()) {
        api.getLocationApi().sendLocation(SettingHelper.getInstance().getUserId()
          , SettingHelper.getInstance().getToken()
          , geofence.getRequestId()
          , geofencingEvent.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER
        ).enqueue(new Callback<Response>() {
          @Override
          public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
            Log.d(LOG_TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
          }

          @Override
          public void onFailure(Call<Response> call, Throwable t) {
            Log.d(LOG_TAG, "Call<Response> " + call + "], Throwable = [" + t + "]");
          }
        });}

    } catch (Throwable e) {
      Log.d(LOG_TAG, "onReceive() called with: context = [" + context + "], e = [" + e + "]");
    }
  }
}
