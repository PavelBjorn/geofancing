package co.placeit.placeitgeo.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.text.TextUtils;
import android.util.Log;

import co.placeit.placeitgeo.data.settings.SettingHelper;
import co.placeit.placeitgeo.service.LocationService;


/**
 * Created by User on 10.07.2017.
 */

public class LocationOnOffReceiver extends BroadcastReceiver {


  private static final String TAG = "LocationOnOffReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {

    if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
      try {
        SettingHelper.init(context.getApplicationContext());
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Intent locationIntent = new Intent(context, LocationService.class);

        if (locationManager == null) return;

        if ((locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
          && !TextUtils.isEmpty(SettingHelper.getInstance().getGeofences())
          && !SettingHelper.getInstance().isServiceActive()) {
          SettingHelper.getInstance().setServiceActive(true);
          context.stopService(locationIntent);
          context.startService(locationIntent);
        } else if ((!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
          SettingHelper.getInstance().setServiceActive(false);
          context.stopService(locationIntent);
        }

        Log.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");

      } catch (Throwable e) {
        Log.d(TAG, "e");
      }
    }
  }
}
