package co.placeit.placeitgeo.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import java.util.Calendar;

import co.placeit.placeitgeo.data.api.Response;
import co.placeit.placeitgeo.data.api.battery.BatteryApiService;
import co.placeit.placeitgeo.data.api.battery.BatteryRequest;
import co.placeit.placeitgeo.data.settings.SettingHelper;
import co.placeit.placeitgeo.service.LocationService;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by User on 01.03.2017.
 */

public class TrackingBatteryService extends BroadcastReceiver {

    private Context mContext;
    private BatteryApiService mBatteryApiService;
    private static final String TAG = "TrackingBatteryService";


    @Override
    public void onReceive(Context context, Intent intent) {
        SettingHelper.init(context.getApplicationContext());
        mContext = context.getApplicationContext();
        Log.d(TAG, "onReceive()");

        mContext.registerReceiver(new BroadcastReceiver() {

                                      @Override
                                      public void onReceive(Context context, Intent intent) {
                                          context.getApplicationContext().unregisterReceiver(this);
                                          int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                                          mContext = null;

                                          Log.d(TAG, "level = " + level + "%");

                                          int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

                                          if (hour >= 19 || hour <= 7) {
                                              return;
                                          }

                                          if (mBatteryApiService == null) {
                                              mBatteryApiService = BatteryApiService.newInstance();
                                          }

                                          mBatteryApiService.getBatteryApi()
                                                  .postBattery(SettingHelper.getInstance().getUserId()
                                                          , SettingHelper.getInstance().getToken()
                                                          , BatteryRequest.create(SettingHelper.getInstance().getKidName(), "" + level)
                                                  ).enqueue(new Callback<Response>() {
                                              @Override
                                              public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                                  Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
                                              }

                                              @Override
                                              public void onFailure(Call<Response> call, Throwable t) {
                                                  Log.d(TAG, t.toString());
                                              }
                                          });
                                      }
                                  }
                , new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        Intent locationIntent = new Intent(context, LocationService.class);
        context.stopService(locationIntent);
        context.startService(locationIntent);
    }

}
