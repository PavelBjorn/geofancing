package co.placeit.placeitgeo.recivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.placeit.placeitgeo.data.settings.SettingHelper;
import co.placeit.placeitgeo.service.LocationService;


/**
 * Created by User on 19.04.2017.
 */

public class RebootReceiver extends BroadcastReceiver {

  private static final String TAG = "RebootReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
      Log.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");
      SettingHelper.init(context);

      Intent alarmIntent = new Intent("co.placeit.placeigeo.GetBatteryLevel");
      PendingIntent startPIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

      AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
      alarmManager.cancel(startPIntent);
      alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 3 * 120 * 10000 * 3, startPIntent);

      Intent gofanceIntent = new Intent(context, LocationService.class);
      context.stopService(gofanceIntent);
      context.startService(gofanceIntent);
    }
  }
}
